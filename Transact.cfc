<cfcomponent>
<cffunction name = "saleTransaction" access = "remote" returnType = "AuthorizationResponse" output="true">               
   <cfargument name = "saleRequest" type="DirectAuthorizationRequest" required="yes">
 	<cflog file="LekkapayAPI" text="[Transact: #Now()# : #saleRequest.orderID#" application="yes"  log="application" type="information">
	<cfscript>
	
    writedump (request);  

	orderObj = createObject ("component", "Order"); 
	orderObj.createDT = Now();
	orderObj.MID = saleRequest.MID;
	orderObj.amount = saleRequest.amount;
	orderObj.currency = saleRequest.currency;
	orderObj.orderIDmerchant = saleRequest.orderID;
	orderObj.customerIP = saleRequest.customerIP;
	orderObj.apiOperation = "PAY";
	orderObj.cardHolderName = saleRequest.cardHolderName;
	orderObj.cardHolderAddress = saleRequest.cardHolderAddress;
	orderObj.cardHolderZipcode = saleRequest.cardHolderZipcode;
	orderObj.cardHolderCity = saleRequest.cardHolderCity;
	orderObj.cardHolderState = saleRequest.cardHolderState;
	orderObj.cardHolderCountryCode = saleRequest.cardHolderCountryCode;
	orderObj.cardHolderPhone = saleRequest.cardHolderPhone;
	orderObj.cardHolderEmail = saleRequest.cardHolderEmail;
	orderObj.cardNumber = saleRequest.cardNumber;
	orderObj.cardSecurityCode = saleRequest.cardSecurityCode;
	orderObj.cardExpireMonth = saleRequest.cardExpireMonth;
	orderObj.cardExpireYear = Right(saleRequest.cardExpireYear,2);
	orderObj.totalAuthorizedAmount = "";
	orderObj.totalCapturedAmount = "";
	orderObj.totalRefundedAmount = "";
	orderObj.csid = "";
	
	writedump(orderObj);
	WriteLog(type="Information", file="LekkapayAPI", text="[Transact] orderObj.orderIDmerchant: #orderObj.orderIDmerchant#");  

   	orderResponseObj = createObject("component", "Order");       	
   	resp = orderResponseObj.Pay(orderObj);
   	writeoutput("<h3>Transact respp</h3>");
   	writedump(resp);
	WriteLog(type="Information", file="LekkapayAPI", text="[Transact] responseObj.status... #resp.status#.  resp.time...#resp.time#"); 
	</cfscript>
   <cfreturn resp>
</cffunction>


<cffunction name = "refundTransaction" access = "remote" returnType = "AuthorizationResponse" output="true">               
   <cfargument name = "refundRequest" type="ReferralDetailedRequest" required="yes">
 	<cflog file="LekkapayAPI" text="[Transact: #Now()# : #refundRequest.orderID#" application="yes"  log="application" type="information">
	
	<cfscript>
    writedump (request);  

	orderObj = createObject ("component", "Order"); 
	orderObj.createDT = Now();
	orderObj.MID = refundRequest.MID;
	orderObj.amount = refundRequest.amount;
	orderObj.currency = "USD";
	orderObj.orderIDmerchant = refundRequest.orderID;
	orderObj.customerIP = "";
	orderObj.apiOperation = "REFUND";
	orderObj.cardHolderName = "";
	orderObj.cardHolderAddress = "";
	orderObj.cardHolderZipcode = "";
	orderObj.cardHolderCity = "";
	orderObj.cardHolderState = "";
	orderObj.cardHolderCountryCode = "";
	orderObj.cardHolderPhone = "";
	orderObj.cardHolderEmail = "";
	orderObj.cardNumber = "";
	orderObj.cardSecurityCode = "";
	orderObj.cardExpireMonth = "";
	orderObj.cardExpireYear = "";
	orderObj.totalAuthorizedAmount = "";
	orderObj.totalCapturedAmount = "";
	orderObj.totalRefundedAmount = "";
	orderObj.csid = "";
	
	writedump(orderObj);
	WriteLog(type="Information", file="LekkapayAPI", text="[Transact] orderObj.orderIDmerchant: #orderObj.orderIDmerchant#");  

   	orderResponseObj = createObject("component", "Order");    
   	resp = orderResponseObj.Refund(orderObj);
	writedump (resp);    
	WriteLog(type="Information", file="LekkapayAPI", text="[Transact] resp.status... #resp.status#");  
    return (resp);
   </cfscript>
   </cffunction>
</cfcomponent>