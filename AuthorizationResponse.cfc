<cfcomponent>
	<cfproperty name="transactionID" type="string" hint="Unique Transaction ID returned to you by Lekkapay" default="0">
	<cfproperty name="time" type="date" hint="Lekkapay transaction date time" default="0">
   	<cfproperty name="orderID" type="string" hint="your unique Order ID passed into Lekkapay" default="">
   	<cfproperty name="status" type="string" hint="Authorized/Not Authorized" default="">
   	<cfproperty name="errorCode" type="string" hint="error code" default="">           
   	<cfproperty name="errorMessage" type="string" hint="error descriptive message" default="">           
	<cfproperty name="information" type="string" hint="optional further info about the payment result" default="">           
   	<cfproperty name="issuerBank" type="string" hint="payment result descriptive message" default="">           
   	<cfproperty name="statementDescriptor" type="any" hint="statement descriptor as shown on cardholders bank statement" default="">           
</cfcomponent>