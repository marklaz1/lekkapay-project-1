<cfcomponent>
	<cfproperty name="createDT" type="date"  default="0" required="yes" hint="order creation date time">
	<cfproperty name="MID" type="numeric"  default="0" required="yes" hint="The merchant's MID">
	<cfproperty name="amount" type="string"  default="0" required="yes" hint="The total amount for the order. This is the net amount plus any surcharge">
	<cfproperty name="currency" type="string"  default="0" required="yes" hint="The currency of the order expressed as an ISO 4217 alpha code, e.g. USD">
	<cfproperty name="orderIDmerchant" type="string" default="" required="no" hint="A unique identifier for this order to distinguish it from any other order you create">
	<cfproperty name="customerIP" type="string" required="yes" hint="customerIP" default="">           
	<cfproperty name="apiOperation" type="string" required="yes" hint="apiOperation" default="">           
	<cfproperty name="cardHolderName" type="string" required="yes" hint="card holder's name" default="">           
	<cfproperty name="cardHolderAddress" type="string" required="no" hint="card holder's address" default="">           
   	<cfproperty name="cardHolderZipcode" type="string" required="no" hint="card holder's zip/postal code" default="">           
   	<cfproperty name="cardHolderCity" type="string" required="no" hint="card holder's city" default="">           
   	<cfproperty name="cardHolderState" type="string" required="no" hint="card holder's state" default="">           
   	<cfproperty name="cardHolderCountryCode" type="string" required="yes" hint="card holder's country code" default="">	
   	<cfproperty name="cardHolderPhone" type="string" required="yes" hint="card holder's phone" default="">	
   	<cfproperty name="cardHolderEmail" type="string" required="yes" hint="card holder's email" default="">			
   	<cfproperty name="cardNumber" type="string" required="yes" hint="cardnumber" default="">		
   	<cfproperty name="cardSecurityCode" type="string" required="yes" hint="card security code (CVV)" default="">		
   	<cfproperty name="cardExpireMonth" type="numeric" required="yes" hint="card expiry month" default="">           
   	<cfproperty name="cardExpireYear" type="numeric" required="yes" hint="card expiry year" default=""> 
   	<cfproperty name="totalAuthorizedAmount" type="string" default="" required="no" hint="The amount that has been successfully authorized for this order.">
   	<cfproperty name="totalCapturedAmount" type="string" default="" required="no" hint="The amount that has been successfully captured for this order">
   	<cfproperty name="totalRefundedAmount" type="string" default="" required="no" hint="The amount that has been successfully refunded for this order">
   
   <cfscript>




	function nvp(stringin,name){
		name = name&"=";
	   	this.stringLen = Len(stringin);
	    this.namePosn = FindNoCase(name,stringin);
	    if (this.namePosn == 0) return "";
	    this.nameUncut = Mid(stringin,this.namePosn, this.stringLen-this.namePosn); 
	    this.nameUncutLen = Len(this.nameUncut);
	    this.nameEndPosn = FindNoCase("&",this.nameUncut);
		if (this.nameEndPosn <= 1) return ("");
	    this.nvp=Left(this.nameUncut,this.nameEndPosn-1); 
	    writeoutput("<hr>this.nvp: "&this.nvp);
	    this.value=Right(this.nvp, Len(this.nvp) - Len(name));
	    return URLDecode(this.value);
	} 
	
	function createTranObj(resp, tranTypeIn, Request) returntype="Transaction" 
	{
	   	orderStatus = nvp(resp,"order.status");
	   	if (orderStatus == "")
			orderStatus =  nvp(resp,"error.cause");  //Validation fail cause
	   	else if (orderStatus == "")
		   orderStatus = Left(resp, 50);  //must be a connection failure   orderID = nvp(resp,"order.id");
	   	gatewayCode = nvp(resp,"response.gatewayCode");
	   	if (gatewayCode == "") 
	   		gatewayCode = (nvp(resp,"error.explanation")); //Validation fail explanation
	   	orderCreationTime = (nvp(resp,"order.creationTime"));
	   	orderLastUpdatedTime = (nvp(resp,"order.lastUpdatedTime"));
	   	cardIssuer = (nvp(resp,"sourceOfFunds.provided.card.issuer"));
	   	orderID = nvp(resp,"order.id"); 
	   	if (orderID == "") orderID=orderIDmerchant;
	   	tranID = nvp(resp,"transaction.id"); 
	   	if (tranID == "") tranID = tranIDreq;  //if no tranaction.id is response from MPGS then set to tranID of request. This happens when validation fails 
	   	tranRef = nvp(resp,"transaction.reference");
//	   	if (tranRef == "") tranRef = Request.;
	   	tranType = nvp(resp,"transaction.type");
	   	if (tranType == "") tranType = tranTypeIn;
	
	   	currency = nvp(resp,"transaction.currency");
	   	if (currency == "") currency = Request.currency;
	   	amount = nvp(resp,"transaction.amount");   
	   	if (amount == "") amount = Request.amount;
	   	acquirerCode = nvp(resp,"response.acquirerCode");
	   	if (acquirerCode == "") acquirerCode = "99";
	
		tranObj = createObject("Transaction");  
		tranObj = createObject("component", "Transaction");  
		tranObj.MID = Request.MID;
		tranObj.orderID = orderID; 	
		tranObj.tranID = tranID;
		tranObj.reference = tranRef;   
		tranObj.tranType = tranType;   
		tranObj.amount = amount;
		tranObj.currency = currency;
		tranObj.receipt = nvp(resp,"transaction.receipt");
		tranObj.result = nvp(resp,"result");  
		tranObj.batch = nvp(resp,"transaction.acquirer.batch");   
		tranObj.settlementDate = nvp(resp,"transaction.acquirer.settlementDate");   
		tranObj.timeOfLastUpdate =  nvp(resp,"timeOfLastUpdate");   
		if (not isdate(tranObj.timeOfLastUpdate)) tranObj.timeOfLastUpdate = Now();
		tranObj.timeOfRecord =  nvp(resp,"timeOfRecord");   
		
		tranObj.orderStatus = orderStatus;   
		tranObj.acquirerCode = acquirerCode;
		tranObj.gatewayCode = gatewayCode;
		tranObj.cardIssuer = cardIssuer;   	
		tranObj.orderRef = orderRef;		
		return tranObj;
	}
	</cfscript>
      
<cffunction name = "Pay" access = "public" returnType = "AuthorizationResponse" output="true">               
   <cfargument name = "Request" type="Order" required="yes">
 	<cflog file="Lekkapay" text="[Order] test log: #Now()#" application="yes"  log="application" type="information">
	<cfscript>
		res = insertOrder(Request);      
	   	WriteLog(type="Information", file="Lekkapay", text="#res#"); 
		if (res eq "SUCCESS")
		{
			WriteLog(type="Information", file="Lekkapay", text="[Order] Order create success");   
	  		orderIDX = getOrderID(Request.orderIDmerchant);
	  		WriteLog(type="Information", file="Lekkapay", text="[Order] OrderIDX: #orderIDX#"); 
   /* 
   	created the Order in db
    (Amount AUTH/CAPTURE/REFUND to be filled AFTER response from upstream.
   
   
	Now is the time to send the request upstream
	From the response fill Transaction in db with response and then update the Amount for Order
	
	Note: for MPGS OrderID is merchant's order ID in the SaleTransaction
	OderRef and TransactionID and TransactionRef all need to be passed as unique values and set up before the call   
   */
   
   
/* we prefill these */
		apiOperation = "PAY";
		merchant = "UBAPAYFUNLNG";
		apiPassword = "1a41730b24323f287209d1887261934b";
		apiUsername = "merchant.UBAPAYFUNLNG";
		
		orderIDmerchant = Request.orderIDmerchant; 
		orderRef = CreateUUID();
		tranIDreq = orderIDX;
		tranRefreq = orderIDmerchant&"-"&orderIDX;   
		tranType = "PAYMENT";  

		payReqUrl = "https://test-gateway.mastercard.com/api/nvp/version/57";
		httpService = new http(method = "POST", charset = "utf-8", url = payReqUrl);
		httpService.addParam(name = "apiOperation", type = "formfield", value = apiOperation);
		httpService.addParam(name = "merchant", type = "formfield", value = merchant);
		httpService.addParam(name = "apiUsername", type = "formfield", value = apiUsername);
		httpService.addParam(name = "apiPassword", type = "formfield", value = apiPassword);
		httpService.addParam(name = "order.id", type = "formfield", value = orderIDmerchant);
		httpService.addParam(name = "order.reference", type = "formfield", value = orderRef);
		httpService.addParam(name = "order.currency", type = "formfield", value = Request.currency);
		httpService.addParam(name = "order.amount", type = "formfield", value = Request.amount);
		httpService.addParam(name = "sourceOfFunds.type", type = "formfield", value = "CARD");
		httpService.addParam(name = "sourceOfFunds.provided.card.number", type = "formfield", value = Request.cardNumber);
		httpService.addParam(name = "sourceOfFunds.provided.card.expiry.month", type = "formfield", value = Request.cardExpireMonth);
		httpService.addParam(name = "sourceOfFunds.provided.card.expiry.year", type = "formfield", value = Request.cardExpireYear);
		httpService.addParam(name = "sourceOfFunds.provided.card.securityCode", type = "formfield", value = Request.cardSecurityCode);
		httpService.addParam(name = "transaction.id", type = "formfield", value = tranIDreq);
		httpService.addParam(name = "transaction.reference", type = "formfield", value = tranRefreq);
		sendResult = httpService.send();
		resp = sendResult.GetPrefix().fileContent.toString();
	   	resp=resp&"&end";
	   	writedump(resp);
	   	
	   	tranObj = createTranObj (resp, tranType, Request);
		writedump (tranObj);
	   	
		WriteLog(type="Information", file="Lekkapay", text="[Order] Transaction object created. Tran type: " & tranObj.tranType & ".  orderID: " & tranObj.orderID & ".  timeOfLastUpdate: " & tranObj.timeOfLastUpdate);    
	    res = tranObj.insertTran(tranObj);
		writeoutput("tran insert res");
	   	writedump(res);
	   
		totalAuthorizedAmount = nvp(resp,"order.totalAuthorizedAmount");
		totalCapturedAmount = nvp(resp,"order.totalCapturedAmount");
		totalRefundedAmount = nvp(resp,"order.totalRefundedAmount");   	
		
		res = updateOrdeAmounts (orderIDX, totalAuthorizedAmount, totalCapturedAmount, totalRefundedAmount);
	 	WriteLog(type="Information", file="Lekkapay", text="[Order] updateOrderAmounts: " & res);      
	
	   	responseObj = createObject("component", "AuthorizationResponse");
	   	responseObj.time = tranObj.timeOfLastUpdate;
		WriteLog(type="Information", file="Lekkapay", text="[Order] responseObj.time: " & responseObj.time);    

	   	responseObj.transactionID = tranID;
	   	responseObj.orderID = orderID;
	   	responseObj.status = tranObj.result;
	   	responseObj.errorCode = acquirerCode;
	   	responseObj.errorMessage = tranObj.gatewayCode;
	   	responseObj.information = orderStatus;
	   	responseObj.issuerBank = tranObj.cardIssuer;
	   	responseObj.statementDescriptor="TBD";
	   	
	   	writedump (responseObj);
	    }
      </cfscript>
   <cfreturn responseObj>
</cffunction>

<cffunction name = "Refund" access = "public" returnType = "AuthorizationResponse" output="true">               
	<cfargument name = "Request" type="Order" required="yes">
 	<cflog file="Lekkapay" text="[Order] test log: #Now()#" application="yes"  log="application" type="information">
	<cfscript>
		res = insertOrder(Request);      
	   	WriteLog(type="Information", file="Lekkapay", text="#res#"); 
		if (res eq "SUCCESS")
		{
			WriteLog(type="Information", file="Lekkapay", text="[Order] Order create success");   
	  		orderIDX = getOrderID(Request.orderIDmerchant);
	  		WriteLog(type="Information", file="Lekkapay", text="[Order] OrderIDX: #orderIDX#"); 
			apiOperation = "REFUND";
			merchant = "UBAPAYFUNLNG";
			apiPassword = "1a41730b24323f287209d1887261934b";
			apiUsername = "merchant.UBAPAYFUNLNG";
			
			orderIDmerchant = Request.orderIDmerchant; 
			orderRef = CreateUUID();
			tranIDreq = orderIDX;
			tranRefreq = orderIDmerchant&"-"&orderIDX;   
			tranType = "REFUND";  
	
			payReqUrl = "https://test-gateway.mastercard.com/api/nvp/version/57";
			httpService = new http(method = "POST", charset = "utf-8", url = payReqUrl);
			httpService.addParam(name = "apiOperation", type = "formfield", value = apiOperation);
			httpService.addParam(name = "merchant", type = "formfield", value = merchant);
			httpService.addParam(name = "apiUsername", type = "formfield", value = apiUsername);
			httpService.addParam(name = "apiPassword", type = "formfield", value = apiPassword);
			httpService.addParam(name = "order.id", type = "formfield", value = orderIDmerchant);
			httpService.addParam(name = "order.reference", type = "formfield", value = orderRef);
			httpService.addParam(name = "transaction.currency", type = "formfield", value = Request.currency);
			httpService.addParam(name = "transaction.amount", type = "formfield", value = Request.amount);
			httpService.addParam(name = "transaction.id", type = "formfield", value = tranIDreq);
			httpService.addParam(name = "transaction.reference", type = "formfield", value = tranRefreq);
			sendResult = httpService.send();
			resp = sendResult.GetPrefix().fileContent.toString();
		   	resp=resp&"&done";
		   	
		   	tranObj = createTranObj (resp, apiOperation, Request);
			writedump (tranObj);
		   	
			WriteLog(type="Information", file="Lekkapay", text="[Order] Transaction object for Refund created: " & tranObj.tranType & "-orderID: " & tranObj.orderID);    
		    res = tranObj.insertTran(tranObj);
			writeoutput("tran insert res");
		   	writedump(res);
		   
			totalAuthorizedAmount = nvp(resp,"order.totalAuthorizedAmount");
			totalCapturedAmount = nvp(resp,"order.totalCapturedAmount");
			totalRefundedAmount = nvp(resp,"order.totalRefundedAmount");   	
			
			res = updateOrdeAmounts (orderIDX, totalAuthorizedAmount, totalCapturedAmount, totalRefundedAmount);
		 	WriteLog(type="Information", file="Lekkapay", text="[Order] updateOrdeAmounts: " & res);      
		
		   	responseObj = createObject("component", "AuthorizationResponse");
		   	responseObj.time = tranObj.timeOfLastUpdate;
		   	responseObj.transactionID = tranID;
		   	responseObj.orderID = orderID;
		   	responseObj.status = tranObj.result;
		   	responseObj.errorCode = acquirerCode;
		   	responseObj.errorMessage = tranObj.gatewayCode;
		   	responseObj.information = orderStatus;
		   	responseObj.issuerBank = tranObj.cardIssuer;
		   	responseObj.statementDescriptor="TBD";
		   	
		   	writedump (responseObj);
		}
	</cfscript>	
	<cfreturn responseObj>
</cffunction>
    
       
<cffunction name="insertOrder" access="public">
   <cfargument name = "orderRequest" type="Order" required="yes">
   <cfscript>
 		WriteLog (type="Information", file="Lekkapay", text="[Order] inserting order...");  
		ccMid =  Mid(ARGUMENTS.orderRequest.cardNumber,6,2);
		ccLeft = "";
		ccnum = "";
		if (Len(ARGUMENTS.orderRequest.cardNumber) ge 2)
		{
			ccLeft = Left(ARGUMENTS.orderRequest.cardNumber, 2);
			ccnum = Left(ARGUMENTS.orderRequest.cardNumber, 6) & "XXXXXX" & Right(ARGUMENTS.orderRequest.cardNumber, 4);
		}
		secCodeLen = Len(ARGUMENTS.orderRequest.cardSecurityCode);
		userVar1 = "";
		userVar2 = "";
		userVar3 = "";
		userVar4 = "";
		csid = Left(ARGUMENTS.orderRequest.csid,499);
		if (isDefined ("ARGUMENTS.orderRequest.userVar1")) userVar1 = Left(ARGUMENTS.orderRequest.userVar1,50);
		if (isDefined ("ARGUMENTS.orderRequest.userVar2")) userVar2 = Left(ARGUMENTS.orderRequest.userVar2,250);
   		if (isDefined ("ARGUMENTS.orderRequest.userVar3")) userVar3 = Left(ARGUMENTS.orderRequest.userVar3,250);
   		if (isDefined ("ARGUMENTS.orderRequest.userVar4")) userVar4 = Left(ARGUMENTS.orderRequest.userVar4,2);
 		WriteLog (type="Information", file="Lekkapay", text="[Order] order details...1. #ARGUMENTS.orderRequest.MID# - 2. #ARGUMENTS.orderRequest.orderIDmerchant# - 
      3. #ARGUMENTS.orderRequest.customerIP# - 4. #ARGUMENTS.orderRequest.amount# -  5. #ARGUMENTS.orderRequest.currency# - 6. #ARGUMENTS.orderRequest.cardHolderName# - 7. #ARGUMENTS.orderRequest.cardHolderEmail# -  -9. {seccode len: #secCodeLen#}[cc:  - 10.#userVar1# - 11.#userVar2# - 12.#userVar3# - 13.#userVar4#");  
    </cfscript>

	<cftry>
    	<cfquery name="qCreateOrder"  result="result">
   	   	INSERT INTO Orders (createDT, MID, apiOperation, orderIDmerchant, customerIP, amount, currency, cardnum, cardHolderName, cardHolderAddress, cardHolderZipcode, cardHolderCity, cardHolderState, cardHolderCountryCode, cardHolderPhone, cardHolderEmail,  userVar1, userVar2, userVar3, userVar4,  amountAuth, amountCapture, amountRefunded) 
            VALUES (
            	<CFQUERYPARAM cfsqltype="cf_sql_timestamp" VALUE="#ARGUMENTS.orderRequest.createDT#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_integer" VALUE="#ARGUMENTS.orderRequest.MID#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.orderRequest.apiOperation#">,
              	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.orderRequest.orderIDmerchant#">,
              	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.orderRequest.customerIP#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_numeric" VALUE="#ARGUMENTS.orderRequest.amount#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.orderRequest.currency#">,
        		<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ccnum#">,
               	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.orderRequest.cardHolderName#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.orderRequest.cardHolderAddress#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.orderRequest.cardHolderZipcode#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.orderRequest.cardHolderCity#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.orderRequest.cardHolderState#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" maxlength="2" VALUE="#Left(ARGUMENTS.orderRequest.cardHolderCountryCode,2)#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.orderRequest.cardHolderPhone#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.orderRequest.cardHolderEmail#">,
        
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#userVar1#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#userVar2#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#userVar3#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#userVar4#">,

            	<CFQUERYPARAM cfsqltype="cf_sql_integer" VALUE="0">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="0">,
            	<CFQUERYPARAM cfsqltype="cf_sql_integer" VALUE="0">
            )
			</cfquery>
      <cfset retval = "SUCCESS">
      <cfcatch type="any">
         <cflog file="Lekkapay" text="[Order] : Inserted order error #cfcatch.Message# #cfcatch.Detail#" application="yes" log="application" type="error">
         <cfset retval = cfcatch>
      </cfcatch>
   </cftry>
   <cfreturn retval>
   </cffunction>
      
      
   <cffunction name="getOrderID" access="public" returntype="any">
   <cfargument name="orderIDmerchant" type="string" required="yes">
   <cfset var qRequests="">

   <cfquery name="qOrders" maxrows="1">
		SELECT ID FROM Orders
        where  orderIDmerchant = <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#ARGUMENTS.orderIDmerchant#">
        ORDER BY ID desc
   </cfquery>
   <cfreturn qOrders.ID>
</cffunction>

<cffunction name="updateOrdeAmounts" access="public">
	<cfargument name="orderIDX" type="numeric" required="true">
   	<cfargument name="amountAuth" type="string" required="true">
   	<cfargument name="amountCapture" type="string" required="true">
   	<cfargument name="amountRefunded" type="string" required="true">
   	<cftry>
    	<cfquery name="qUpdateAuthReqId"> 
            UPDATE Orders 
                  SET  updateDT = <CFQUERYPARAM cfsqltype="cf_sql_timestamp" VALUE="#Now()#">,
                  amountAuth = <CFQUERYPARAM cfsqltype="cf_sql_numeric" VALUE="#ARGUMENTS.amountAuth#">,
                  amountCapture = <CFQUERYPARAM cfsqltype="cf_sql_numeric" VALUE="#ARGUMENTS.amountCapture#">,
                  amountRefunded = <CFQUERYPARAM cfsqltype="cf_sql_numeric" VALUE="#ARGUMENTS.amountRefunded#">
                  WHERE ID = <CFQUERYPARAM cfsqltype="CF_SQL_INTEGER" VALUE="#ARGUMENTS.orderIDX#">
      	</cfquery>
      	<cfset retval = "SUCCESS">
      	<cfcatch type="any">
        	<cflog file="GBN_API" text="[updateAuthReqId] : #cfcatch.Message# #cfcatch.Detail#" application="yes" log="application" type="error">
        	<cfset retval = cfcatch>
      	</cfcatch>
   	</cftry>
   	<cfreturn retval>
</cffunction>


<cffunction name="updateOrdeCreationTime" access="public">
   	<cfargument name="orderIDX" type="numeric" required="true">
   	<cfargument name="creationTime" type="numeric" required="true">
   	<cftry>
    	<cfquery name="qUpdateAuthReqId"> 
            UPDATE Orders 
                  SET  updateDT = <CFQUERYPARAM cfsqltype="cf_sql_timestamp" VALUE="#Now()#">,
                  creationTime = <CFQUERYPARAM cfsqltype="cf_sql_timestamp" VALUE="#ARGUMENTS.creationTime#">
                  WHERE ID = <CFQUERYPARAM cfsqltype="CF_SQL_INTEGER" VALUE="#ARGUMENTS.orderIDX#">
        </cfquery>
      	<cfset retval = "SUCCESS">
      	<cfcatch type="any">
        	<cflog file="GBN_API" text="[updateAuthReqId] : #cfcatch.Message# #cfcatch.Detail#" application="yes" log="application" type="error">
         	<cfset retval = cfcatch>
      	</cfcatch>
   	</cftry>
   	<cfreturn retval>
</cffunction>
</cfcomponent>