<cfcomponent>
    <cfproperty name="MID" required="yes" type="numeric" hint="your unique websiteID as assigned to you by Globill" default="0">
    <cfproperty name="password" required="yes" type="string" hint="your unique password as assigned to you by Globill" default="">
    <cfproperty name="orderID" required="yes" type="string" hint="your unique Order ID" default="">
    <cfproperty name="customerIP" type="string" required="yes" hint="customerIP" default="">           
    <cfproperty name="amount" type="numeric" required="yes" hint="amount" default="0">           
    <cfproperty name="currency" type="string" required="yes" hint="currency" default="USD">           
    <cfproperty name="cardHolderName" type="string" required="yes" hint="card holder's name" default="">           
    <cfproperty name="cardHolderAddress" type="string" required="no" hint="card holder's address" default="">           
    <cfproperty name="cardHolderZipcode" type="string" required="no" hint="card holder's zip/postal code" default="">           
    <cfproperty name="cardHolderCity" type="string" required="no" hint="card holder's city" default="">           
    <cfproperty name="cardHolderState" type="string" required="no" hint="card holder's state" default="">           
    <cfproperty name="cardHolderCountryCode" type="string" required="yes" hint="card holder's country code" default="">	
    <cfproperty name="cardHolderPhone" type="string" required="yes" hint="card holder's phone" default="">	
    <cfproperty name="cardHolderEmail" type="string" required="yes" hint="card holder's email" default="">			
    <cfproperty name="cardNumber" type="string" required="yes" hint="cardnumber" default="">		
    <cfproperty name="cardSecurityCode" type="string" required="yes" hint="card security code (CVV)" default="">		
    <cfproperty name="cardExpireMonth" type="numeric" required="yes" hint="card expiry month" default="">           
    <cfproperty name="cardExpireYear" type="numeric" required="yes" hint="card expiry year" default="">           
    <cfproperty name="AVSPolicy" type="string" required="no" hint="AVS Policy - leave empty" default="">           
    <cfproperty name="FSPolicy" type="string" required="no" hint="Fraud Screening (FS) Policy" default="">           
    <cfproperty name="Secure3DAcsMessage" type="string" required="no" hint="3D Acs message" default="">           
    <cfproperty name="Secure3DCheckTransactionID" type="string" required="no" hint="3D check transactionID" default="">
    <cfproperty name="userVar1" type="string" required="no" hint="additional information used in transaction mail/ used for MaxMind minFraud parameters" default="">           
    <cfproperty name="userVar2" type="string" required="no" hint="additional information used in transaction mail" default="">
    <cfproperty name="userVar3" type="string" required="no" hint="only to be used by special agreement with Globill" default="">
    <cfproperty name="userVar4" type="string" required="no" hint="only to be used by special agreement with Globill" default="">   
    <cfproperty name="csid" type="string" required="no" hint="value obtained using previously provided Javascript" default="">   
</cfcomponent>