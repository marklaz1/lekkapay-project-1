<cfcomponent>
<cfproperty name="orderID" type="string" default="" required="no" hint="Unique identifier for this order to distinguish it from any other order.">
<cfproperty name="orderRef" type="uuid" default="" required="no" hint="Unique ref for this order. internally generated.">
<cfproperty name="tranID" type="numeric" default="" required="no" hint="Unique identifier for this transaction to distinguish it from any other transaction on the order.">
<cfproperty name="amount" type="string"  default="0" required="yes" hint="The total amount for the transaction">
<cfproperty name="currency" type="string"  default="0" required="yes" hint="The currency of the transaction expressed as an ISO 4217 alpha code, e.g. USD">
<cfproperty name="reference" type="string"  default="0" required="yes" hint="">
<cfproperty name="tranType" type="string" default="" required="no" hint="Indicates the type of action performed on the order">
<cfproperty name="receipt" type="string"  default="0" required="yes" hint="">
<cfproperty name="result" type="string"  default="0" required="yes" hint="">
<cfproperty name="batch" type="string"  default="0" required="yes" hint="">
<cfproperty name="settlementDate" type="string"  default="0" required="yes" hint="">
<cfproperty name="gatewayCode" type="string"  default="0" required="yes" hint="">
<cfproperty name="acquirerCode" type="string"  default="0" required="yes" hint="">
<cfproperty name="timeOfRecord" type="date"  default="0" required="yes" hint="">
<cfproperty name="timeOfLastUpdate" type="date"  default="0" required="yes" hint="">
<cfproperty name="orderStatus" type="string"  default="0" required="yes" hint="">
<cfproperty name="cardIssuer" type="string"  default="0" required="yes" hint="">

<cffunction name="insertTran" access="public">
<cfargument name = "tran" type="Transaction" required="yes">
	<cflog file="Lekkapay" text="[Transaction] inserting..."  application="yes"  log="application" type="information">
       <cfoutput>yipee: #ARGUMENTS.tran.orderID#</cfoutput>
   		<cfdump var="#ARGUMENTS#">
<!--- add orderStatus, acquirerDate --->
   	<cftry>
    	<cfquery name="qCreateTran"  result="retval">
   	   	INSERT INTO Transactions (createDT, orderIDmerch, orderRef, MID, tranID,  amount, currency, reference, tranType, receipt, result, batch, settlementDate, gatewayCode, acquirerCode, timeOfRecord, timeOfLastUpdate, orderStatus, cardIssuer) 
            VALUES (
            	<CFQUERYPARAM cfsqltype="cf_sql_timestamp" VALUE="#Now()#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.tran.orderID#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.tran.orderRef#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.tran.MID#">,
               	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.tran.tranID#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.tran.amount#"  null="#NOT isNumeric(ARGUMENTS.tran.amount)#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.tran.currency#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.tran.reference#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.tran.tranType#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.tran.receipt#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.tran.result#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.tran.batch#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.tran.settlementDate#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.tran.gatewayCode#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.tran.acquirerCode#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_timestamp" VALUE="#ARGUMENTS.tran.timeOfRecord#" null="#NOT isDate(ARGUMENTS.tran.timeOfRecord)#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_timestamp" VALUE="#ARGUMENTS.tran.timeOfLastUpdate#" null="#NOT isDate(ARGUMENTS.tran.timeOfLastUpdate)#">,
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.tran.orderStatus#">,      
            	<CFQUERYPARAM cfsqltype="cf_sql_varchar" VALUE="#ARGUMENTS.tran.cardIssuer#">          
            	)
		</cfquery>
			
		<cfdump var="#retval#">
      	<cfset retval = "SUCCESS">
      	<cfcatch type="any">
        	<cflog file="Lekkapay" text="[Transaction] insert transaction : #cfcatch.Message# #cfcatch.Detail#" application="yes" log="application" type="error">
         	<cfset retval = cfcatch>
      	</cfcatch>
   	</cftry>
   	<cfreturn retval>
   </cffunction>
</cfcomponent>